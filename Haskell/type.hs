type Nome = String
type Idade = Int
type Linguagem = String
type Pessoa = (Nome, Idade, Linguagem)

pessoa :: Pessoa
pessoa = ("joao", 21, "Haskell")

my_fst :: Pessoa -> Nome
my_fst(nome, idade, linguagem) = nome