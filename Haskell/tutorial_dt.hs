-- Comentário
{-
Outro comentário
-}

--Import
import Data.List

--Listas

numList = [1,2,3,4,5]
rangeList = [1..5]
alphaList = ['a'..'z']
evenNums = [2,4..20]
oddAlpha = ['a','c'..'z']
sumNumList = sum numList
listContainsFive = elem 5 numList
moreNumList = [6..10]
newNumList = numList ++ moreNumList

maxNum = maximum newNumList
minNum = minimum newNumList

myZip = zipWith (+) [1,2,3,4,5] [6,7,8,9,10]

infOdds = [1,3..]
takeOdds = take 20 infOdds
repOdds = replicate 20 takeOdds
cycleZip = cycle myZip
takeCycle = take 50 $ cycle [1,2,3,4,5,6,7,8,9,10]

filterZip = filter (>5) myZip
whileZip = takeWhile (<=8) myZip

mapList = map (*2) [1..10]

unordList = [545,2,34,87, 3, 897, 56, 13]
ordList = sort unordList

multiList = foldr (*) 2[3,4,5,6]
-- 3 * ( 4 * ( 5 * ( 6 * 2)))

mInusList =  foldl (-) 2 [3,4,5,6]
-- (((((2) -6) -5) -4 ) -3)

sumFold = foldl (+) 1[1..100]

consList = 1 : 2 : 3 : 4 : 5 : []

listFunc = [x * y | x <- [1..5], y <- [1..5], x * y `mod` 2 == 0]

hello = "HELLo, World!"

onlyUpperCase :: [Char] -> [Char]
onlyUpperCase s = [c | c <- s, elem c ['A'..'Z']]

main = do
    let s = onlyUpperCase "Heaven Embrace the Light and Love"
    print(s)
