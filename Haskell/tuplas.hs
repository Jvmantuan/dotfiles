func :: (Int, Int) -> (Int, Int) -> (Int, Int)
func (a,b) (c,d) = (a+c, b+d)

nomes :: (String, String, String)
nomes = ("João", "Haskell", "Xmonad")

select_first (first, _, _) = first
select_second (_, second, _) = second
select_third (_, _, third) = third
