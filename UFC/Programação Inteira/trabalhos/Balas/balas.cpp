#include <iostream>
#include <fstream>
#include <new>
#include <string>
#include "balas.hpp"

using std::ifstream, std::string, std::nothrow, std::cerr, std::getline, std::endl;

int main() {

    ifstream inFile;
    string line, c;
    bool *sub; // Diz se a variável foi alterada para o seu complemento
    int *var; // Representa as variáveis em uso
    int num_var, num_constraints; // Variáveis relacionadas às quantidades do problema, ex (numero de variaveis, numero de restrições...)
    int *b; // Vetor b (lado direito da equação)
    int *fo; // Vetor para a função objetivo
    int *slack; // Vetor para as variáveis de folga

    inFile.open("entrada1.txt");

    if(!inFile.is_open()){
	cerr << "Erro ao abrir o arquivo.";
	return false;
    }

    getline(inFile, line); //Obtem o número de variáveis 
    num_var = stoi(line);
    fo = new(nothrow) int[num_var];

    sub = new(nothrow) bool[num_var];
    var = new(nothrow) int[num_var];
    
    for (int i = 0; i < num_var; i++){
	sub[i] = false;	
	var[i] = 0;
    }
    
    getline(inFile, line); // Pega o número de restrições
    num_constraints = stoi(line);
    b = new(nothrow) int[num_constraints];

    getline(inFile, line); // Pega os coeficientes da função objetivo
    
    // Passando os valores da função objetivo para um vetor
    int j = 0; c.clear();
    for(int i = 0; i < line.length() - 1; i++){
	fo[j] = 0;
	if(line[i] != ' ')
	    c += line[i];	    
	else {
	    fo[j] = stoi(c);
	    c.clear();
	    j++;
	}
    }
    fo[j] = stoi(c); // Passando o último valor para o vetor
    
    getline(inFile, line); // Pega os coeficientes do lado direito da equação
     
    // Passando os valores do lado direito da equação para um vetor
    j = 0; c.clear();
    for(int i = 0; i < line.length() - 1; i++){
	b[j] = 0;
	if(line[i] != ' ')
	    c += line[i];	    
	else {
	    b[j] = stoi(c);
	    c.clear();
	    j++;
	}
    }
    b[j] = stoi(c); // Passando o último valor para o vetor
    
    int **matrix = new_matrix(num_constraints * 2, num_var);

    // Passando os elementos da restrição para a matriz
    int k = 0;
    while(getline(inFile, line)) {
	c.clear();
	j = 0;
	for(int i = 0; i < line.length(); i++) {
	    if(line[i] != ' ')
		c += line[i];
	    else {
		matrix[k][j] = stoi(c);
		c.clear();
		j++; 
	    }
	}
	matrix[k][j] = stoi(c);
	k++;	
    } 
    c.clear();

    slack = new(nothrow) int[num_constraints];

    for(int i = 0; i < num_constraints; i++)
	slack[i] = 1;

    // Fechando os arquivos da memória
    inFile.close();
    
    cout << "funcao obj: ";
    printVector(fo, num_var);
    cout << "lado b: ";
    printVector(b, num_constraints);

    cout << endl;
    printMatrix(matrix, num_constraints, num_var);

    free(b); free(sub); free(var); 
}
