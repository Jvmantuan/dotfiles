#include <new>
#include <iostream>

using std::cout, std::nothrow, std::cerr;

int **new_matrix(int m, int n) {
    int **matrix;

    matrix = (int **) calloc(m, sizeof(int *));
    for(int i = 0; i < m; i++){
	matrix[i] = (int *) calloc(n, sizeof(int));
	if(matrix[i] == nullptr){
	    cerr << "Erro de alocação.";
	    return nullptr;
	}
    }
    return matrix;
}

void printMatrix(int **matrix, int rows, int cols) {
    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < cols; j++) {
	    cout << matrix[i][j] << " ";
	}
	cout << '\n';
    }
}

template<typename T> void printVector(T *vector, int size) {
    for(int j = 0; j < size; j++)
	cout << vector[j] << " ";
    cout << '\n';
}
