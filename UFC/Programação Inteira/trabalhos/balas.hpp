#include <new>
#include <iostream>

using std::cout, std::nothrow, std::cerr;

int **new_matrix(int m, int n) {
    int **matrix;

    matrix = (int **) calloc(m, sizeof(int *));
    for(int i = 0; i < n; i++){
	matrix[i] = (int *) calloc(n, sizeof(int));
	if(matrix[i] == nullptr){
	    cerr << "Erro de alocação.";
	    return nullptr;
	}
    }
    return matrix;
}

void printMatrix(int **matrix=nullptr, int *matrix2 = nullptr,  int rows, int cols) {
    if(rows == 1 && matrix2 != nullptr)
	for(int i = 0; i < cols; i++)
	    cout << matrix[rows][j];
    else if(rows > 1 && matrix != nullptr)
	for(int i = 0; i < rows; i++) {
	    for(int j = 0; i < cols; j++) {
		cout << matrix[i][j] << " ";
		}
	    cout << '\n';
	}
}
