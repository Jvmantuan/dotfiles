from libqtile import bar, widget
import os
import socket

colors = [["#292d3e", "#292d3e"], # panel background
          ["#434758", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#ff383b", "#ff383b"], # border line color for other tab and odd widgets
          ["#668bd7", "#668bd7"], # color for the even widgets
          ["#e1acff", "#e1acff"]] # window name

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

widget_defaults = dict(
    font='Mononoki Nerd Font' ,
    fontsize=12,
    padding=3,
    background=colors[0]
)
extension_defaults = widget_defaults.copy()

main_bar = bar.Bar(
   [ 
                widget.Sep(
                    linewidth=0,
                    padding=6,
                    foreground=colors[2],
                    background=colors[0]
               ),
                widget.Image(
                    filename="/home/joao/.config/qtile/icons/python.png",
                    mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn('dmenu_run -p "Run: "')}
                    ),
                widget.GroupBox(
                    font='Mononoki Nerd Font' ,
                    fontsize=12,
                    margin_y=3,
                    margin_x=1,
                    borderwidth=2,
                    padding=3,
                    padding_x=5,
                    padding_y=5,
                    active=colors[2],
                    inactive=colors[2],
                    foreground=colors[2],
                    background=colors[0],
                    highlight_method="block",
                    highlight_color=colors[4],
                    rounded=False,
                    this_current_screen_border = colors[4],
                    this_screen_border = colors [4],
                    other_current_screen_border = colors[0],
                    other_screen_border = colors[0],
                    hide_unused = True
                ),
                widget.Prompt(
                    prompt=prompt,
                    font="SF UI Display",
                    padding=10,
                    foreground=colors[3],
                    background=colors[1],
                    ),
                widget.Sep(
                       linewidth = 0,
                       padding = 20,
                       ),

                widget.WindowName(
                    foreground=colors[6],
                    background=colors[0],
                    padding=0
                    ),
                widget.Systray(
                    background = colors[0],
                    padding = 10
                    ),
                widget.Net(
                       interface = "wlan0",
                       format = '{down} ↓↑ {up}',
                       padding = 10
                       ),
                widget.TextBox(
                       text = '',
                       padding = 5,
                       font="Font Awesome 5 Free",
                       ),
                widget.Volume(
                       padding = 10,
                       ),
		widget.Backlight(
		    backlight_name="intel_backlight",
		    brightness_file="actual_brightness",
		    format="☼ {percent: 2.0%}"
		),
                widget.Battery(
                    battery=0,
                    font="Font Awesome 5 Free",
                    charge_char='',
                    padding=5,
                    discharge_char='',
                    format='{char} {percent:2.0%}',
                    ),
		    widget.Memory(
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e htop')},
                       padding = 5
                       ),

                widget.CurrentLayout(
                        foreground = colors[2],
                       background = colors[5],
                       padding = 5
                        ),
                widget.Clock(
                        format='%d/%m/%Y %H:%M %p',
                        ),
                 widget.Sep(
                       linewidth = 0,
                       padding = 10,
                       ),
            ],
            size=20,
            opacity=0.95
)
