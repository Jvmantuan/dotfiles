#! /bin/bash

#nitrogen --restore &
feh --bg-fill /home/joao/Imagens/space.jpg &
picom -b &
xmodmap -e "keymap 117 = Return" &
xmodmap -e "keymapp 112 = BackSpace" &
xmodmap -e "keycode 135 = ccedilla" &
nm-applet &
mixer -q set Master unmute &
