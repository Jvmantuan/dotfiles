from libqtile.config import Key, Screen, Group, Drag, Click, Match
from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook
import os
import socket
import subprocess
from typing import List  # noqa: F401
from bar import main_bar, widget_defaults

mod = "mod4"
myTerminal = "alacritty"
myConfig = "/home/joao/.config/qtile/config.py"


#### BACKLIGHT ####
def brightness(action):
    def f(qtile): 
        br = open("/sys/class/backlight/intel_backlight/actual_brightness", "r") 
        if round(float(br.read())/1000) > 10:
            qtile.cmd_spawn("xbacklight -" + action + " 10")
        else:
            qtile.cmd_spawn(f"xbacklight -{action} 1")

    return f
            
keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down()),
    Key([mod], "j", lazy.layout.up()),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down()),
    Key([mod, "control"], "j", lazy.layout.shuffle_up()),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next()),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "control"], "m", lazy.window.toggle_maximize()),
    Key([mod, "control"], "t", lazy.spawn(myTerminal)),

    Key([mod], "h",
             lazy.layout.grow(),
             lazy.layout.increase_nmaster(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
    Key([mod], "l",
             lazy.layout.shrink(),
             lazy.layout.decrease_nmaster(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
    Key([mod, "shift"], "space",
             lazy.layout.rotate(),
             lazy.layout.flip(),
             desc='Switch which side main pane occupies (XmonadTall)'
             ),


    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod], "w", lazy.window.kill()),

    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),
    Key([mod], "r", lazy.spawn("dmenu_run -p 'Run: '")),

    # Google Chrome
    Key([mod], "f", lazy.spawn("firefox")),

    # Nautilus File Explorer
    Key([mod], "e", lazy.spawn("nautilus")),

    # Terminal commands
    Key([mod, "control"], "p", lazy.spawn(myTerminal + " -e nvim " + myConfig)),

    # Multimidia keys
    Key(
        [], "XF86AudioRaiseVolume",
        lazy.spawn("amixer -c 0 sset Master 2+ unmute")
    ),
    Key(
        [], "XF86AudioLowerVolume",
        lazy.spawn("amixer -c 0 sset Master 2dB- unmute")
    ),
    Key(
        [], "XF86AudioMute",
        lazy.spawn("amixer -q set Master toggle")
    ),
    Key([], 'XF86MonBrightnessUp',   lazy.function(brightness("inc"))), 
    Key([], 'XF86MonBrightnessDown', lazy.function(brightness("dec"))),
]

#### GROUPS ####
group_names = [("Terminal", {'layout': 'monadtall'}),
               ("Navegador", {
                   'layout': 'monadtall',
                   'matches': [
                       Match(wm_class=["firefox"]),
                       Match(wm_class=["Google-chrome"]),]
                   }),

               ("Desenvolvimento", {'layout': 'monadtall'}),
               ("Documentos", {'layout': 'monadtall'}),
               ("Sistema", {'layout': 'monadtall'}),
               ("Outros", {
                   'layout': 'monadtall',
                   'persist': 'False',
                   'init': 'False',
                   }),]

groups = [Group(name, **kwargs) for name, kwargs in group_names ]

for i in groups:
    print(i)

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name)))


##### DEFAULT THEME SETTINGS FOR LAYOUTS #####
layout_theme = {"border_width": 2,
                "margin": 10,
                "border_focus": "e1acff",
                "border_normal": "1D2330"}    

layouts = [
    layout.Max(**layout_theme),
    layout.Stack(num_stacks=2, **layout_theme),
    # layout.Bsp(),
    # layout.Columns(),
    # layout.Matrix(),
    layout.MonadTall(**layout_theme),
    # layout.MonadWide(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
]

#### COLORS ####

colors = [["#292d3e", "#292d3e"], # panel background
          ["#434758", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#8d62a9", "#8d62a9"], # border line color for other tab and odd widgets
          ["#668bd7", "#668bd7"], # color for the even widgets
          ["#e1acff", "#e1acff"]] # window name

extension_defaults = widget_defaults.copy()

#### SCREENS ####
main_screen = Screen(top=main_bar)
screens = [main_screen]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'nautilus'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

#### STARTUP APPLICATIONS ####

@hook.subscribe.startup
def start_once():
    home = os.path.expanduser('~')
    subprocess.call(['/home/joao/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
