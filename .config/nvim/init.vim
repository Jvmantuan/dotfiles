set nocompatible
filetype off

syntax on
set nu
set ruler
set mouse=a
set showmatch
set linebreak
set hidden
set nobackup
set nowritebackup


set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

    Plugin 'VundleVim/Vundle.vim'
    Plugin 'morhetz/gruvbox'
    Plugin 'sheerun/vim-polyglot'
    Plugin 'itchyny/lightline.vim'
    Plugin 'tpope/vim-fugitive'
    Plugin 'neoclide/coc.nvim'
    Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
    Plugin 'joshdick/onedark.vim'
    Plugin 'scrooloose/nerdtree'
    Plugin 'mattn/emmet-vim'
    Plugin 'neovimhaskell/haskell-vim'
    Plugin 'itchyny/vim-haskell-indent'
    Plugin 'dart-lang/dart-vim-plugin'
    Plugin 'natebosch/vim-lsc'
    Plugin 'natebosch/vim-lsc-dart'
    Plugin 'nvie/vim-flake8'
    Plugin 'drewtempelmeyer/palenight.vim'

call vundle#end()


set hlsearch
set smartcase
set autoindent
set shiftwidth=4
set softtabstop=4
set noshowmode
set laststatus=2

let g:lightline = {
    \ 'colorscheme': 'palenight',
    \ 'active': {
    \   'left': [ [ 'mode', 'paste' ],
    \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
    \ },
    \ 'component_function': {
    \   'gitbranch': 'FugitiveHead'
    \ },
    \ }

if !has('gui_running')
  set t_Co=256
endif

filetype plugin indent on

if (has("nvim"))
  "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif

if (has("termguicolors"))
  set termguicolors
endif

if exists('*complete_info')
  inoremap <expr> <TAB> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<TAB>"
else
  inoremap <expr> <TAB> pumvisible() ? "\<C-y>" : "\<C-g>u\<TAB>"
endif

if executable('bash-language-server')
  au User lsp_setup call lsp#register_server({
        \ 'name': 'bash-language-server',
        \ 'cmd': {server_info->[&shell, &shellcmdflag, 'bash-language-server start']},
        \ 'whitelist': ['sh'],
        \ })
endif

let g:lsc_auto_map = v:true
let g:haskell_enable_quantification = 1
set background=dark
colorscheme palenight
