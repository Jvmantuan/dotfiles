-- Imports
import XMonad
import Data.Monoid
import System.Exit
import System.IO
import Data.Tree

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig
import XMonad.Util.Scratchpad
import XMonad.Util.WorkspaceCompare

import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops 
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.FadeWindows
import XMonad.Hooks.ServerMode
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat)

import XMonad.Layout.Decoration
import XMonad.Layout.ShowWName
import XMonad.Layout.SimplestFloat
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.ShowWName
import XMonad.Layout.Spacing
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))

import XMonad.Actions.CycleWS
--import XMonad.Actions.GridSelect
import qualified XMonad.Actions.TreeSelect as TS
import XMonad.Actions.MouseResize
--import XMonad.Actions.TreeSelect (tsDefaultConfig, treeselectWorkspace, toWorkspaces)

myTerminal :: String
myTerminal = "alacritty"

myBrowser :: String
myBrowser = "google-chrome-stable"

myFileManager :: String
myFileManager = "nautilus"

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myClickJustFocuses :: Bool
myClickJustFocuses = False

myBorderWidth :: Dimension
myBorderWidth = 2

myModMask :: KeyMask
myModMask = mod4Mask

myFont :: String
myFont = "xft:Mononoki Nerd Font:bold:size=9:antialias=true:hinting=true"

-- Border colors for unfocused and focused windows, respectively.
myNormalBorderColor  = "#292d3e"
myFocusedBorderColor = "#bbc5ff"

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

--mygridConfig :: p -> GSConfig Window
--mygridConfig colorizer = (buildDefaultGSConfig myColorizer)
--    { gs_cellheight   = 40
--   , gs_cellwidth    = 200
--    , gs_cellpadding  = 6
--    , gs_originFractX = 0.5
--    , gs_originFractY = 0.5
--    , gs_font         = myFont
--    }

--spawnSelected' :: [(String, String)] -> X ()
--spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
--    where conf = def
--                   { gs_cellheight   = 40
--                   , gs_cellwidth    = 200
--                   , gs_cellpadding  = 6
--                   , gs_originFractX = 0.5
--                  , gs_originFractY = 0.5
--                   , gs_font         = myFont
--                   }

myAppGrid = [ ("Audacity", "audacity")
                 , ("Deadbeef", "deadbeef")
                 , ("Emacs", "emacsclient -c -a emacs")
                 , ("Firefox", "firefox")
                 , ("Geany", "geany")
                 , ("Geary", "geary")
                 , ("Gimp", "gimp")
                 , ("Kdenlive", "kdenlive")
                 , ("LibreOffice Impress", "loimpress")
                 , ("LibreOffice Writer", "lowriter")
                 , ("OBS", "obs")
                 , ("PCManFM", "pcmanfm")
                 , ("Google-chrome", "google-chrome")
                 ]

-- Key bindings. Add, modify or remove key bindings here.
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- launch a terminal
    [ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)

    -- Change the Brightness
    , ((0,		    0x1008FF02), spawn "xbacklight -inc 10")
    , ((0,		    0x1008FF03), spawn "xbacklight -dec 10")

    -- Change the volume
    , ((0,		    0x1008FF12), spawn "amixer -c 0 sset Master toggle") -- Mute
    , ((0,                  0x1008FF11), spawn "amixer -c 0 sset Master 2- unmute") -- Lower
    , ((0,                  0x1008FF13), spawn "amixer -c 0 sset Master 2+ unmute") -- Raise
    , ((modm .|. shiftMask, xK_t     ), treeselectAction myTSConfig)
    -- launch dmenu
    , ((modm,               xK_p     ), spawn "dmenu_run")

    -- Launch Browser
    , ((modm,		    xK_f     ), spawn myBrowser)
    
    -- Print Screen
    , ((0,		    xK_Print ), spawn "sleep 0.2; scrot -s -e 'mv $f /home/joao/Imagens/Screenshots/'")

    -- launch gmrun
    , ((modm .|. shiftMask, xK_p     ), spawn myFileManager)

    -- close focused window
    , ((modm .|. shiftMask, xK_c     ), kill)

     -- Rotate through the available layout algorithms
    , ((modm,               xK_space ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,               xK_n     ), refresh)

    -- Move focus to the next window
    --, ((modm,               xK_Tab   ), moveTo Next (WSIs notSP))

    -- Move focus to the next window
    , ((modm,               xK_j     ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm,               xK_Tab     ), windows W.focusUp  )

	-- Move focus to the master window
    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm,               xK_Return), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )

    -- Shrink the master area
    , ((modm,               xK_h     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm,               xK_l     ), sendMessage Expand)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    --, ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    --, ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- , ((modm              , xK_b     ), sendMessage ToggleStruts)

    -- Quit xmonad
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))

    -- Restart xmonad
    , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")

    -- Run xmessage with a summary of the default keybindings (useful for beginners)
    , ((modm .|. shiftMask, xK_slash ), spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))

    ]

    ++
	 [((m .|. myModMask, k), windows $ f i)
         | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++
 
    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. myModMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
    
myKeysP = [ ("M-.", nextScreen)  -- Switch focus to next monitor
           , ("M-,", prevScreen)  -- Switch focus to prev monitor
          ]


    
------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm .|. shiftMask, button1), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    ]

------------------------------------------------------------------------
-- Layouts:

myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Ubuntu:bold:size=60"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#000000"
    , swn_color             = "#FFFFFF"
    }

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

tall    = renamed [Replace "tall"]
	$ mySpacing 8
	$ ResizableTall 1 (3/100) (1/2) []
floats  = renamed [Replace "floats"]
        $ limitWindows 20 simplestFloat
tabs    = renamed [Replace "tabs"]
        $ tabbed shrinkText myTabConfig
  where
    myTabConfig = def { fontName            = "xft:Mononoki Nerd Font:regular:pixelsize=11"
                      , activeColor         = "#292d3e"
                      , inactiveColor       = "#3e45e"
                      , activeBorderColor   = "#292d3e"
                      , inactiveBorderColor = "#292d3e"
                      , activeTextColor     = "#ffffff"
                      , inactiveTextColor   = "#d0d0d0"
                      }

myLayout = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
    where
	myDefaultLayout = tall ||| Mirror tall ||| noBorders tabs ||| Full 

xmobarEscape = concatMap doubleLts
    where
	doubleLts '<' = "<<"
	doubleLts x   = [x]

myWorkspaces :: [String]
myWorkspaces = clickable . (map xmobarEscape)
    -- $ ["1",    "2",      "3",       "4",        "5",    "6", "7", "8", "9"]
    $ ["term", "browser", "system", "documents", "vbox", "vid"]
  where
        clickable l = [ "<action=xdotool key super+" ++ show (n) ++ ">" ++ ws ++ " </action>" |
		      (i,ws) <- zip [1..6] l,
		      let n = i ]

treeselectAction :: TS.TSConfig (X ()) -> X ()
treeselectAction a = TS.treeselectAction a
  [ Node (TS.TSNode "+ Development" "IDEs and Editors/Terminals" (return ()))
    [ Node (TS.TSNode "Visual Studio Code" "An Editor from Microsoft" (spawn "code")) []
    , Node (TS.TSNode "Doom Emacs" "An Evil distribution from Emacs" (spawn "emacsclient -c -a emacs")) []
    , Node (TS.TSNode "Geany" "Geany is a powerful, stable and lightweight programmer's text editor" (spawn "geany")) []
    , Node (TS.TSNode "NeoVim" "NeoVim is a more powerfull version of Vim" (spawn "nvim")) []
    ]
  ]

myTSConfig :: TS.TSConfig a
myTSConfig = TS.TSConfig {  TS.ts_hidechildren = True
                              , TS.ts_background   = 0xdd292d3e
                              , TS.ts_font         = myFont
                              , TS.ts_node         = (0xffd0d0d0, 0xff202331)
                              , TS.ts_nodealt      = (0xffd0d0d0, 0xff292d3e)
                              , TS.ts_highlight    = (0xffffffff, 0xff755999)
                              , TS.ts_extra        = 0xffd0d0d0
                              , TS.ts_node_width   = 200
                              , TS.ts_node_height  = 20
                              , TS.ts_originX      = 0
                              , TS.ts_originY      = 0
                              , TS.ts_indent       = 80
                              , TS.ts_navigate     = myTreeNavigation
                              }

--myTreeNavigation :: M.Map (KeyMask, KeySym) (TreeSelect a (Maybe a))

myTreeNavigation = M.fromList
    [ ((0, xK_Escape), TS.cancel)
    , ((0, xK_Return), TS.select)
    , ((0, xK_space),  TS.select)
    , ((0, xK_Up),     TS.movePrev)
    , ((0, xK_Down),   TS.moveNext)
    , ((0, xK_Left),   TS.moveParent)
    , ((0, xK_Right),  TS.moveChild)
    , ((0, xK_k),      TS.movePrev)
    , ((0, xK_j),      TS.moveNext)
    , ((0, xK_h),      TS.moveParent)
    , ((0, xK_l),      TS.moveChild)
    , ((0, xK_o),      TS.moveHistBack)
    , ((0, xK_i),      TS.moveHistForward)
    ]

myManageHook = composeAll
	[ className =? "MPlayer"        --> doFloat
	, className =? "Google-chrome"  --> doShift (myWorkspaces !! 1)
	, className =? "firefox"        --> doShift (myWorkspaces !! 1)
	, className =? "Gimp"           --> doFloat
	, resource  =? "desktop_window" --> doIgnore
	, resource  =? "kdesktop"       --> doIgnore ]

------------------------------------------------------------------------
-- Event handling
myEventHook = mempty

------------------------------------------------------------------------
-- Status bars and logging

-- Startup hook
myStartupHook :: X ()
myStartupHook = do
	spawn "trayer --edge top --align right --widthtype request --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 --tint 0x292d3e --height 22 &"
	spawnOnce "nm-applet &"
	spawnOnce "volumeicon &"
	--spawn "feh --bg-fill /home/joao/Imagens/neon.jpg &"
	spawn "nitrogen --restore"
	--spawn "xcompmgr -f -c -C -n -D 3 &"
	spawn "picom -c &"
	spawn "xmodmap -e 'keycode 117 = Return' &"
	spawn "xmodmap -e 'keycode 112 = BackSpace' &"
	spawn "xmodmap -e 'keycode 135 = ccedilla' &"

main = do
    xmproc0 <- spawnPipe "xmobar -x 0 /home/joao/.config/xmobar/xmobarrc" 
    xmproc1 <- spawnPipe "xmobar -x 1 /home/joao/.config/xmobar/xmobarrc1" 
    xmproc2 <- spawnPipe "xmobar -x 2 /home/joao/.config/xmobar/xmobarrc2"

    xmonad $ docks $ ewmh def { 
	manageHook = ( isFullscreen --> doFullFloat ) <+> myManageHook <+> manageDocks,
      -- simple stuff
	terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         =  myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = myLayout,
        handleEventHook    = fullscreenEventHook <+> myEventHook,
        logHook            = workspaceHistoryHook <+> dynamicLogWithPP xmobarPP 
    	    { ppOutput = \x -> hPutStrLn xmproc0 x  >> hPutStrLn xmproc1 x >> hPutStrLn xmproc2 x
	    , ppCurrent = xmobarColor "#c3e88d" "" . wrap "[" "]"
	    , ppVisible = xmobarColor "#c3e88d" ""                -- Visible but not current workspace
            , ppHidden = xmobarColor "#82AAFF" "" . wrap "*" ""   -- Hidden workspaces in xmobar
            , ppHiddenNoWindows = xmobarColor "#ff6365" ""        -- Hidden workspaces (no windows)
            , ppTitle = xmobarColor "#b3afc2" "" . shorten 0     -- Title of active window in xmobar
            , ppSep =  "<fc=#666666> |</fc>"                     -- Separators in xmobar
            , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"  -- Urgent workspace
            , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]

	    },
        startupHook        = myStartupHook

    } `additionalKeysP` myKeysP 

-- | Finally, a copy of the default bindings in simple textual tabular format.
help :: String
help = unlines ["The default modifier key is 'alt'. Default keybindings:",
    "",
    "-- launching and killing programs",
    "mod-Shift-Enter  Launch xterminal",
    "mod-p            Launch dmenu",
    "mod-Shift-p      Launch gmrun",
    "mod-Shift-c      Close/kill the focused window",
    "mod-Space        Rotate through the available layout algorithms",
    "mod-Shift-Space  Reset the layouts on the current workSpace to default",
    "mod-n            Resize/refresh viewed windows to the correct size",
    "",
    "-- move focus up or down the window stack",
    "mod-Tab        Move focus to the next window",
    "mod-Shift-Tab  Move focus to the previous window",
    "mod-j          Move focus to the next window",
    "mod-k          Move focus to the previous window",
    "mod-m          Move focus to the master window",
    "",
    "-- modifying the window order",
    "mod-Return   Swap the focused window and the master window",
    "mod-Shift-j  Swap the focused window with the next window",
    "mod-Shift-k  Swap the focused window with the previous window",
    "",
    "-- resizing the master/slave ratio",
    "mod-h  Shrink the master area",
    "mod-l  Expand the master area",
    "",
    "-- floating layer support",
    "mod-t  Push window back into tiling; unfloat and re-tile it",
    "",
    "-- increase or decrease number of windows in the master area",
    "mod-comma  (mod-,)   Increment the number of windows in the master area",
    "mod-period (mod-.)   Deincrement the number of windows in the master area",
    "",
    "-- quit, or restart",
    "mod-Shift-q  Quit xmonad",
    "mod-q        Restart xmonad",
    "mod-[1..9]   Switch to workSpace N",
    "",
    "-- Workspaces & screens",
    "mod-Shift-[1..9]   Move client to workspace N",
    "mod-{w,e,r}        Switch to physical/Xinerama screens 1, 2, or 3",
    "mod-Shift-{w,e,r}  Move client to screen 1, 2, or 3",
    "",
    "-- Mouse bindings: default actions bound to mouse events",
    "mod-button1  Set the window to floating mode and move by dragging",
    "mod-button2  Raise the window to the top of the stack",
    "mod-button3  Set the window to floating mode and resize by dragging"]
