set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

    Plugin 'tpope/vim-surround'
    Plugin 'terryma/vim-multiple-cursors'
    Plugin 'itchyny/lightline.vim'
    Plugin 'VundleVim/Vundle.vim'
    Plugin 'tpope/vim-fugitive'
    Plugin 'git://git.wincent.com/command-t.git'
    Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
    Plugin 'joshdick/onedark.vim'
    Plugin 'scrooloose/nerdtree'
    Plugin 'mattn/emmet-vim'
    Plugin 'neoclide/coc.nvim', {'branch': 'release'}
    Plugin 'neovimhaskell/haskell-vim'
    Plugin 'itchyny/vim-haskell-indent'
    Plugin 'dart-lang/dart-vim-plugin'
    Plugin 'natebosch/vim-lsc'
    Plugin 'natebosch/vim-lsc-dart'
    Plugin 'drewtempelmeyer/palenight.vim'
    Plugin 'itchyny/vim-gitbranch'
    Plugin 'nvie/vim-flake8'

call vundle#end()            " required
filetype plugin indent on    " required

set number	
set showmatch	
set linebreak
set mouse=a
set hidden
set nobackup
set nowritebackup
  
set hlsearch	
set smartcase	
set ignorecase	
set incsearch
 
set autoindent	
set shiftwidth=4
set smartindent
set smarttab
set softtabstop=4

syntax on

set ruler	 
set undolevels=1000
set backspace=indent,eol,start
 
set noshowmode 
set laststatus=2

let g:lightline = {
    \ 'colorscheme': 'palenight',
    \ 'active': {
    \   'left': [ [ 'mode', 'paste' ],
    \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
    \ },
    \ 'component_function': {
    \   'gitbranch': 'FugitiveHead'
    \ },	
    \ }

if !has('gui_running')
    set t_Co=256
endif

if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

if (has("nvim"))
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif

if (has("termguicolors"))
  set termguicolors
endif

if exists('*complete_info')
  inoremap <expr> <TAB> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<TAB>"
else
  inoremap <expr> <TAB> pumvisible() ? "\<C-y>" : "\<C-g>u\<TAB>"
endif

if executable('bash-language-server')
  au User lsp_setup call lsp#register_server({
        \ 'name': 'bash-language-server',
        \ 'cmd': {server_info->[&shell, &shellcmdflag, 'bash-language-server start']},
        \ 'whitelist': ['sh'],
        \ })
endif

let g:lsc_auto_map = v:true

set background=dark
colorscheme palenight

" Italics for my favorite color scheme
let g:palenight_terminal_italics=1
